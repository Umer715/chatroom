// dom
var chatList = document.querySelector('.chat-list'); 
var newChatForm = document.querySelector('.new-chat');
var newFormName = document.querySelector('.new-name');
var updateMsg = document.querySelector('.update-msg');
var element = document.getElementsByClassName('list-group');


class ChatUI {
    constructor(list){
        this.list = '';
        this.list = list;
    }
    render(data){
        const when = dateFns.distanceInWordsToNow(
            new Date(data.created_at), 
            { addSuffix: true}
        );
        const html = `
        <li class="list-group-item" >
        <span class='username'>${data.username} : </span>
        <p class='message'>${data.message}</p>
        <div class='time'>${when}</div>
        </li>`;
       this.list.innerHTML += html; 
    }
}
//chat update 
newChatForm.addEventListener('click' , e => {
    e.preventDefault();
    const message = newChatForm.message.value.trim();
    chatroom.addChat(message)
    .then(() => newChatForm.reset())
    .catch(err => console.log(err));
});

//name update 
newFormName.addEventListener('submit', e => {
    e.preventDefault();
    const newName = newFormName.name.value.trim();
    chatroom.updateName(newName);

    //reset the field
    newFormName.reset();
    updateMsg.innerText = `Your name has been changed to ${newName}`;
    setTimeout(() => updateMsg.innerText = `Named as ${newName}`, 3000)
});
//clear chats
function clearChats(){
    jQuery('.list-group').remove(); 
}

const username = localStorage.username ? localStorage.username : 'ANONYMOUS';

var chatroom = new Chatroom('general', username);
var chatUI = new ChatUI(chatList);

//class instance
//render chat
//room changes clear the chats
 chatroom.getChats((data) => {
    chatUI.render(data);
 });


 

